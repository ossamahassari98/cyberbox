package scheduler

import (
	"encoding/json"
	"fmt"
	"log"
	"orchestrator/config"
	"orchestrator/containers"
	"orchestrator/kafka"
	"sync"
	"time"

	kafka_go "github.com/segmentio/kafka-go"

	"github.com/robfig/cron"
)

type Scanner struct {
	Address  []string
	Scanners []config.ImageContainer
	Elipse   string
	location *time.Location
	checker  *cron.Cron
	Producer *kafka.Producer
}

func NewScanner(address []string, scanners []config.ImageContainer, cronFrequency string, loc *time.Location, producer *kafka.Producer) *Scanner {
	log.Println("Launching Scanner service....")
	return &Scanner{
		Address:  address,
		Scanners: scanners,
		location: loc,
		Elipse:   cronFrequency,
		checker:  cron.NewWithLocation(loc),
		Producer: producer,
	}
}

// deprecated
func (e *Scanner) AddJobFunc(s string, fn func()) {
	e.checker.AddFunc(s, fn)
}

func (e *Scanner) StartCron() {
	e.checker.Start()
}

func (e *Scanner) ListEntries() string {
	var s string
	v := e.checker.Entries()
	for i, val := range v {
		s += fmt.Sprintf("Schedule %d : %v | ", i, val.Schedule)
	}
	return s
}

func (e *Scanner) StartScan() {
	var wg sync.WaitGroup

	for _, _ = range e.Address {
		for _, scan := range e.Scanners {
			wg.Add(1)
			go func(scan config.ImageContainer, producer *kafka.Producer) {
				defer wg.Done()
				_, buff, err := containers.CreateNewContainer(scan.Image, scan.Command)
				if err != nil {
					log.Println(err)
				}

				jsonData, err := json.Marshal(buff)
				if err != nil {
					log.Fatalf("Failed to marshal JSON payload: %v", err)
				}

				msg := kafka_go.Message{Value: []byte(jsonData)}
				fmt.Println(msg)
				producer.Produce(msg)

			}(scan, e.Producer)
		}
	}
	//wait for all go routines to complete
	wg.Wait()
}
