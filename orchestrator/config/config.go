package config

import (
	"log"

	"github.com/spf13/viper"
)

type ImageContainer struct {
	Image   string   `mapstructure:"image"`
	Command []string `mapstructure:"command"`
}

func NewConfig() (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigType("yaml")
	v.SetConfigName("settings")
	v.AddConfigPath("config/")
	v.AddConfigPath("config\\")
	err := v.ReadInConfig()
	if err != nil {
		log.Fatal("error on parsing configuration file")
		return nil, err
	}

	return v, nil
}
