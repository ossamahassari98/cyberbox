package kafka

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/segmentio/kafka-go"
)

type Producer struct {
	Server     string
	Topic      string
	Partition  int
	Connection *kafka.Conn
}

func NewProducer(server string, topic string, partition int) (*Producer, error) {
	conn, err := kafka.DialLeader(context.Background(), "tcp", server, topic, partition)
	if err != nil {
		log.Fatal("failed to dial leader:", err)
	}
	log.Println("Connecting Producer....")
	return &Producer{
		Server:     server,
		Topic:      topic,
		Partition:  partition,
		Connection: conn,
	}, nil
}

func (p *Producer) Produce(msg kafka.Message) error {
	//log.Println("Sending :", msg)
	p.Connection.SetWriteDeadline(time.Now().Add(10 * time.Second))
	_, err := p.Connection.WriteMessages(msg)
	if err != nil {
		return fmt.Errorf("failed to write messages: %v", err)
	}

	if err := p.Connection.Close(); err != nil {
		return fmt.Errorf("failed to close writer: %v", err)
	}
	return nil
}
