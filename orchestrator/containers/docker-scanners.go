package containers

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
)

func CreateNewContainer(image string, cmd []string) (string, string, error) {
	//cli, err := client.NewEnvClient()
	cli, err := client.NewClientWithOpts(
		client.FromEnv,
		client.WithAPIVersionNegotiation(),
	)
	if err != nil {
		fmt.Println("Unable to create docker client")
		panic(err)
	}

	//hostBinding := nat.PortBinding{
	//	HostIP:   "0.0.0.0",
	//	HostPort: "8000",
	//}
	//containerPort, err := nat.NewPort("tcp", "80")
	//if err != nil {
	//panic("Unable to get the port")
	//}

	//portBinding := nat.PortMap{containerPort: []nat.PortBinding{hostBinding}}

	err = pullImage(cli, image)
	if err != nil {
		fmt.Printf("Failed to pull Docker image: %v\n", err)
		return "", "", err
	}

	//container config

	var container_config *container.Config
	if len(cmd) != 0 {
		container_config = &container.Config{
			Image: image,
			Cmd:   cmd,
		}
	} else {
		container_config = &container.Config{
			Image: image,
		}
	}

	ctx := context.Background()

	cont, err := cli.ContainerCreate(
		ctx,
		container_config,
		&container.HostConfig{
			AutoRemove: true, // Set AutoRemove option to remove the container after it exits
		},
		/*&container.HostConfig{
			PortBindings: portBinding,
		}*/nil, nil, "")
	if err != nil {
		panic(err)
	}

	err = cli.ContainerStart(ctx, cont.ID, types.ContainerStartOptions{})
	if err != nil {
		return "", "", err
	}

	// Wait for the container to exit
	statusCh, errCh := cli.ContainerWait(context.Background(), cont.ID, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			fmt.Printf("Failed to wait for container: %v\n", err)
			return cont.ID, "", err
		}
	case <-statusCh:
	}

	// Retrieve the container logs
	logs, err := getContainerLogs(cli, cont.ID)
	if err != nil {
		fmt.Printf("Failed to get container logs: %v\n", err)
		return cont.ID, "", err
	}

	// Print the container logs
	//_, err = io.Copy(os.Stdout, logs)
	//if err != nil {
	//	fmt.Printf("Failed to print container logs: %v\n", err)
	//	return cont.ID, "", err
	//}

	// Produce output
	buf := new(strings.Builder)
	_, err = io.Copy(buf, logs)
	if err != nil {
		return cont.ID, "", err
	}
	// check errors
	//fmt.Println(buf.String())

	fmt.Println("Container executed successfully!")

	return cont.ID, buf.String(), nil
}

func pullImage(cli *client.Client, imageName string) error {
	ctx := context.Background()

	// Define the options for image pulling
	options := types.ImagePullOptions{
		All:          false,
		RegistryAuth: "",
	}

	// Pull the image from Docker Hub
	out, err := cli.ImagePull(ctx, imageName, options)
	if err != nil {
		return err
	}

	defer out.Close()

	// Read the output to consume any progress or error messages
	_, err = io.Copy(os.Stdout, out)
	if err != nil {
		return err
	}

	return nil
}

func getContainerLogs(cli *client.Client, containerID string) (io.ReadCloser, error) {
	ctx := context.Background()

	options := types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
	}

	logs, err := cli.ContainerLogs(ctx, containerID, options)
	if err != nil {
		return nil, err
	}

	return logs, nil
}
