## Orchestrator

This projecct must define a configuration file in config/settings.yaml:

```yaml
---
scheduler:
  cron: "@every 1m"
  location: "Europe/Paris"
  scripts_images: 
      - image: "images/nmap-nse"
        command: ["sh", "-c", "nmap -sV --script /usr/share/nmap/scripts/nmap-vulners/ 127.0.0.1"]
      - image: "images/tsunami"
      - image: "images/ncrack"
      #- image: "ossi98/nmap-nse"
      #  command: ["sh", "-c", "nmap -sV --script /usr/share/nmap/scripts/nmap-vulners/ 127.0.0.1"]

  address: ["127.0.0.1","192.168.X.X","192.168.X.X"]

kafka:
  bootstrap-server: "localhost:9092"
  topic: "cyberbox-clientID"
  partition: 0

```

At this time while i am writing this doc, container of the orchestrator can reach kafka cluster (must define network to communicate with kafka). The connection work when running this command :
```bash
go run main.go
```