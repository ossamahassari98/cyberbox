package main

import (
	"fmt"
	"log"
	"orchestrator/config"
	"orchestrator/kafka"
	"orchestrator/scheduler"
	"time"

	"github.com/spf13/viper"
)

func main() {
	stop := make(chan bool)
	log.Println("Starting orchestrator...")
	go run()
	//go fmt.Println(conf.AllSettings())
	//go infinitLoop()
	//go containers.CreateNewContainer("ossi98/nmap-nse", []string{"sh", "-c", "nmap -sV --script /usr/share/nmap/scripts/nmap-vulners/ 127.0.0.1"}) //[]string{})

	<-stop // Program will stay in execution until explicitly stopped
}

func run() {
	ch := make(chan *kafka.Producer)
	/**
	 * Config
	 */
	conf, err := config.NewConfig()
	if err != nil {
		panic(err)
	}

	/**
	 * Location Time
	 */
	loc, err := time.LoadLocation(conf.GetString("scheduler.location"))
	if err != nil {
		fmt.Println(err)
	}

	var imagesStruct []config.ImageContainer

	jobTime := conf.GetString("scheduler.cron")
	address := conf.GetStringSlice("scheduler.address")
	err = conf.UnmarshalKey("scheduler.scripts_images", &imagesStruct)
	if err != nil {
		log.Fatalf("Failed to unmarshal scripts_images: %v", err)
	}

	fmt.Println(loc, jobTime, address, imagesStruct)

	/**
	 * Connecting Producer to Kafka broker
	 */
	//producer, err := kafka.NewProducer(conf.GetString("kafka.bootstrap-server"), conf.GetString("kafka.topic"), conf.GetInt("kafka.partition"))
	//if err != nil {
	//	 log.Fatal(err)
	//}
	//producer.Produce(kafka_go.Message{Value: []byte("buff")})

	go startProducer(ch, conf)
	producer := <-ch

	/**
	 * Starting Cron Service
	 **/
	cron := scheduler.NewScanner(address, imagesStruct, jobTime, loc, producer)
	cron.AddJobFunc(jobTime, cron.StartScan)
	cron.StartCron()
	cron.ListEntries()
}

func startProducer(ch chan<- *kafka.Producer, conf *viper.Viper) {
	producer, err := kafka.NewProducer(conf.GetString("kafka.bootstrap-server"), conf.GetString("kafka.topic"), conf.GetInt("kafka.partition"))
	if err != nil {
		log.Fatal(err)
	}
	ch <- producer
}

func infinitLoop() {
	for {
		time.Sleep(time.Second) // Sleep for a second before the next iteration
	}
}
