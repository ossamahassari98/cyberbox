## Set up envirenment :

Change directory to the sub-project you choose, then create a vitrual envirenement:

```bash
py -3 -m venv .venv
```

Then activate the venv :
```bash
# powershell
.venv\Scripts\Activate.ps1

# command prompt
.venv\Scripts\activate.bat
```

Now you can install project dependencies using *requirements.txt* file :
```bash
pip install -r requirements.txt
```

If a dependency is added to the project :
```bash
pip freeze > requirements.txt
```