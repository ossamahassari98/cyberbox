import sh    
  
  
"""
for octet3 in range(0,256):  
    net_class = "192.168."+str(octet3)
    for octet4 in range(1,256):  
        ip = f"{net_class}."+str(octet4)  

        try:  
            sh.ping(ip, "-c 1",_out="/dev/null")  
            print("PING ",ip , "OK")  
        except sh.ErrorReturnCode_1:  
            print ("PING ", ip, "FAILED")
"""

for num in range(10,40):  
    ip = "192.168.0."+str(num)  
  
    try:  
        sh.ping(ip, "-c 1",_out="/dev/null")  
        print ("PING ",ip , "OK")  
    except sh.ErrorReturnCode_1:  
        print ("PING ", ip, "FAILED") 