FROM debian:stable

ENV NMAP_VULNERS=/usr/share/nmap/scripts/nmap-vulners/

RUN set -eux; apt update -y && apt upgrade -y && apt install  git nmap -y

RUN git clone https://github.com/vulnersCom/nmap-vulners.git && mv ./nmap-vulners /usr/share/nmap/scripts/

