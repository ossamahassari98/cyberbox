# cyberbox

## Table of content

- [Documentation](Intro.md)

## Architecture project

![alt Architecture](./docs/Architecture.png)

## Maintainers

- [Ossama HASSARI](https://gitlab.com/ossamahassari98)
- Lyza MESSADENE
- Denesh RAMA DOSS
- Aalae EL SHARKAWY
- Yanis ALLOUANE
- Marvin SABIN
- Idir ABDELLI
- Walid ARAB-OUSFAR
- Mathys GROUILLER

## License

[MIT License](https://gitlab.com/ossamahassari98/cyberbox/-/blob/main/LICENSE)
