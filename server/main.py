from json import loads
import json  
from kafka import KafkaConsumer  
"""
def main():
    # To consume from fintechexplained-topic
    consumer = KafkaConsumer('cyberbox-clientID',
                            enable_auto_commit=True,
                            bootstrap_servers=['127.0.0.1:9092'],
            value_deserializer=lambda m: json.loads(m.decode('utf-8')))

    for message in consumer:
        print(message.topic)
        print(message.partition)
        print(message.offset)
        print(message.value)



if __name__ == "__main__":
    main()

"""
# To consume from fintechexplained-topic
consumer = KafkaConsumer('cyberbox-clientID',
                        enable_auto_commit=True,
                        bootstrap_servers=['localhost:9092'],
        value_deserializer=lambda m: json.loads(m.decode('utf-8')))

for message in consumer:
    print(message.topic)
    print(message.partition)
    print(message.offset)
    print(message.value)
    # TODO: define the formatting logic for the data recived and then send it to elastic search
